package com.example.happy;

import android.util.Log;

import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.object.human.FacialExpressions;
import com.aldebaran.qi.sdk.object.human.Human;
import com.aldebaran.qi.sdk.object.humanawareness.HumanAwareness;

import java.util.List;

/**
 * Observe the facial expression of the first human seen by the robot.
 *
 * Created by Susana Sanchez Restrepo on 07/03/2019.
 * Softbank Robotics
 * susana.sanchez@softbankrobotics.com
 */

class FacialExpressionObserver {

    // Store the facial expression listener.
    private OnFacialExpressionChangedListener listener;
    // Store the HumanAwareness service.
    private HumanAwareness humanAwareness;
    // Store the observed facial expression.
    private FacialExpressions observedFacialExpression;
    // Store the last facial expression
    private FacialExpressions lastFacialExpression;

    private static final String TAG = "FacialExpressionObserve";

    /**
     * Start the observation.
     * @param qiContext the qiContext
     */
    public void startObserving(QiContext qiContext) {

        // Get the HumanAwareness service.
        humanAwareness = qiContext.getHumanAwareness();

        // Retrieve the humans around and update the observed emotion.
        List<Human> humansAround = humanAwareness.getHumansAround();
        Log.i(TAG, humansAround.size() + " human(s) around.");

        if (!humansAround.isEmpty()){
            observedFacialExpression = humansAround.get(0).getFacialExpressions();
            updateObservedFacialExpression(humansAround);
        }

        // Update the observed facial expression when the humans around change.
        humanAwareness.addOnHumansAroundChangedListener(this::updateObservedFacialExpression);
    }

    /**
     * Stop the observation.
     */
    public void stopObserving() {
        // Clear observed emotion.
        List<Human> humansAround = humanAwareness.getHumansAround();
        clearObservedFacialExpression(humansAround);

        // Remove listeners on HumanAwareness.
        if (humanAwareness != null) {
            humanAwareness.removeAllOnHumansAroundChangedListeners();
            humanAwareness = null;
        }
    }


    /**
     * Set the listener.
     * @param listener the listener
     */
    public void setListener(OnFacialExpressionChangedListener listener) {
        this.listener = listener;
    }


    private void updateObservedFacialExpression(List<Human> humansAround) {

        if (!humansAround.isEmpty()) {
            //clearObservedFacialExpression(humansAround);
            // Update observed human
            Human observedHuman = humansAround.get(0);

            // Get last facial expression
            lastFacialExpression = observedHuman.getFacialExpressions();

            // Notify the listener when facial expression changes
            observedHuman.addOnFacialExpressionsChangedListener(facialExpression-> {
                if (facialExpression != lastFacialExpression) {
                    lastFacialExpression = facialExpression;
                    if (listener != null) {
                        listener.OnFacialExpressionChanged(facialExpression);
                    }
                }
            });
        }

    }

    private void clearObservedFacialExpression(List<Human> humansAround) {
        Human observedHuman = humansAround.get(0);
        // Remove listeners on observed facial expression.
        if (observedHuman != null) {
            if(observedFacialExpression != null){
                observedHuman.removeAllOnFacialExpressionsChangedListeners();
                observedFacialExpression = null;
            }
        }
    }
}
