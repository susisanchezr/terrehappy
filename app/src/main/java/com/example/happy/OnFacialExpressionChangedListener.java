package com.example.happy;

import com.aldebaran.qi.sdk.object.human.FacialExpressions;

/**
 * Listener used to notify when the facial expression changes.
 */
interface OnFacialExpressionChangedListener {

    void OnFacialExpressionChanged(FacialExpressions facialExpressions);

}
