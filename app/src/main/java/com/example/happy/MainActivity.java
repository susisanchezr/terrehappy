package com.example.happy;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;

import com.aldebaran.qi.Future;
import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.QiSDK;
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks;
import com.aldebaran.qi.sdk.builder.AnimateBuilder;
import com.aldebaran.qi.sdk.builder.AnimationBuilder;
import com.aldebaran.qi.sdk.builder.SayBuilder;
import com.aldebaran.qi.sdk.builder.TakePictureBuilder;
import com.aldebaran.qi.sdk.design.activity.RobotActivity;
import com.aldebaran.qi.sdk.object.actuation.Animate;
import com.aldebaran.qi.sdk.object.actuation.Animation;
import com.aldebaran.qi.sdk.object.camera.TakePicture;
import com.aldebaran.qi.sdk.object.conversation.Say;
import com.aldebaran.qi.sdk.object.human.AttentionState;
import com.aldebaran.qi.sdk.object.human.FacialExpressions;
import com.aldebaran.qi.sdk.object.human.Human;
import com.aldebaran.qi.sdk.object.human.SmileState;
import com.aldebaran.qi.sdk.object.humanawareness.HumanAwareness;
import com.aldebaran.qi.sdk.object.image.EncodedImage;
import com.aldebaran.qi.sdk.object.image.EncodedImageHandle;
import com.aldebaran.qi.sdk.object.image.TimestampedImageHandle;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.List;

import static com.aldebaran.qi.sdk.object.human.AttentionState.LOOKING_AT_ROBOT;

/**
 * TerreHAPPY : encourage humans around the robot to smile, take pictures of them and share via e-mail.
 *
 * Created by Susana Sanchez Restrepo on 07/03/2019.
 * Softbank Robotics
 * susana.sanchez@softbankrobotics.com
 */

public class MainActivity extends RobotActivity implements RobotLifecycleCallbacks, OnFacialExpressionChangedListener {

    // Store the HumanAwareness service.
    private HumanAwareness humanAwareness;
    // The QiContext provided by the QiSDK.
    private QiContext qiContext;
    // Create TAG for logs
    private static final String TAG = "MainActivity";

    // An image view used to show the picture.
    private ImageView pictureView;
    private Bitmap pictureBitmap;
    private ImageView emotionView;

    // Store the facial expression observer.
    private FacialExpressionObserver facialExpressionObserver;

    // Store the Animate action.
    private Animate animate;
    private MediaPlayer mediaPlayer;
    private Say say;

    // variable to lock activities when robot is in action
    private boolean robotInAction;
    private int counterSmile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        QiSDK.register(this, this);

        pictureView = findViewById(R.id.picture_view);
        emotionView = findViewById(R.id.emotionView);
        Button button = findViewById(R.id.button);

        // Create the facial expression observer and listen to it.
        facialExpressionObserver = new FacialExpressionObserver();
        facialExpressionObserver.setListener(this);

        button.setOnClickListener(v -> {
            if (qiContext != null) {
                sharePicture();
            }
        });
    }

    @Override
    public void onRobotFocusGained(QiContext qiContext) {

        // Store the provided QiContext.
        this.qiContext = qiContext;

        // Start the facial expression observation.
        facialExpressionObserver.startObserving(qiContext);

        //Get the HumanAwareness service from the QiContext.
        humanAwareness = qiContext.getHumanAwareness();

        say = SayBuilder.with(qiContext)
                .withText("Hi! I'm Paisa. \\style=joyful\\ Show me your smiling face and I'll take a picture")
                .build();

        say.addOnStartedListener(()-> robotInAction = true);

        Future<Void> smileFuture = say.async().run();
        smileFuture.andThenConsume(ignore->{
            // act on success
            robotInActionFalse(); });

    }


    @Override
    protected void onStart() {
        super.onStart();
        mediaPlayer = MediaPlayer.create(this, R.raw.flash);
    }

    @Override
    protected void onStop() {
        mediaPlayer.release();
        mediaPlayer = null;
        super.onStop();
    }


    @Override
    public void OnFacialExpressionChanged(FacialExpressions facialExpressions) {
        SmileState smileState;
        smileState = facialExpressions.getSmile();
        Log.i(TAG, "Smile state changed: " + smileState + " lock " + robotInAction);

        // Create an animation.
        Animation animationTakePic = AnimationBuilder.with(qiContext) // Create the builder with the context.
                .withResources(R.raw.nicereaction_a001) // Set the animation resource.
                .build(); // Build the animation.

        // Create an animate action.
        animate = AnimateBuilder.with(qiContext) // Create the builder with the context.
                .withAnimation(animationTakePic) // Set the animation.
                .build(); // Build the animate action.

        // Add an on started listener to the animate action.
        animate.addOnStartedListener(() -> {
            String message = "Animation started.";
            Log.i(TAG, message);


        });

        List<Human> humansAround = humanAwareness.getHumansAround();

        if (!humansAround.isEmpty())
        {
            AttentionState attention = humansAround.get(0).getAttention();
            Log.i(TAG, "ATTENTION STATE ---> " + attention);

            if (counterSmile >= 2){
                counterSmile = 0;
            }

            if (!robotInAction)  {

                switch (smileState) {
                    case BROADLY_SMILING:
                        Log.i(TAG, "Smiles COUNTER : " + counterSmile);
                        if (counterSmile == 0){
                            takePicture();
                            runOnUiThread(() -> emotionView.setImageResource(R.drawable.sheep));
                        }
                        counterSmile = counterSmile + 1;

                        break;
                    case SMILING:
                        Log.i(TAG, "Smiles COUNTER : " + counterSmile);
                        if (counterSmile == 0){
                            takePicture();
                            runOnUiThread(() -> emotionView.setImageResource(R.drawable.cactus_happy));
                        }
                        counterSmile = counterSmile + 1;
                        break;
                    case NOT_SMILING:
                        //runOnUiThread(() -> pictureView.setImageResource(R.drawable.smile));

                        if (attention == LOOKING_AT_ROBOT) {

                            runOnUiThread(() -> {
                                emotionView.setImageResource(R.drawable.zombie);
                                pictureView.setImageResource(android.R.color.transparent);
                            });

                            say = SayBuilder.with(qiContext)
                                    .withText("\\vol=100\\ Why so serious? \\style=joyful\\ Smile for me!")
                                    .build();

                            say.addOnStartedListener(()-> robotInAction = true);

                            Future<Void> smileFuture = say.async().run();
                            smileFuture.andThenConsume(ignore->{
                                // act on success
                                robotInActionFalse(); });
                        }
                        else {
                            runOnUiThread(() -> emotionView.setImageResource(R.drawable.unknown));
                        }
                        break;

                    case UNKNOWN:
                        runOnUiThread(() -> {
                            emotionView.setImageResource(R.drawable.unknown);
                            pictureView.setImageResource(android.R.color.transparent);
                        });

                        break;}
            }

        }
        else
        {
            Log.e(TAG, "No humans around ---> " + humansAround.size());
        }
    }


    private void sharePicture(){
        try {
            robotInAction = true;
            File file = new File(this.getExternalCacheDir(),"yourBeautifulSmile.png");
            FileOutputStream fOut = new FileOutputStream(file);
            pictureBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            intent.putExtra(Intent.EXTRA_SUBJECT, "TerraHappy by Paisa: Your beautiful smile");
            intent.putExtra(Intent.EXTRA_TEXT, "Hi happy person, " +
                    "thanks for being part of the TerraHappy. " +
                    "We hope you enjoy your picture and keep smiling during the day. " +
                    "Feliz dia, " +
                    "Paisa" );
            intent.setType("message/rfc822");
            //intent.setType("text/plain");
            //startActivity(intent);
            startActivity(Intent.createChooser(intent, "Share image by e-mail"));
            robotInAction = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void robotInActionFalse(){
        robotInAction = false;
        Log.i(TAG, "robotInActionFalse  ");
    }

    private void animateWithSound() {

        // Launch camera flash sound
        mediaPlayer.start();
        // Launch animation

        Future<Void> animateFuture = animate.async().run();
        animateFuture.thenConsume(future -> {
            if (future.isSuccess()){
                robotInActionFalse();
            }
            else if (future.hasError()) {
                Log.e(TAG, "Animation finished with error.", future.getError());
            }
        });
    }

    private void takePicture() {

        // Check that the Activity owns the focus.
        if (qiContext == null) {
            return;
        }

        robotInAction = true;

        // Launch animation and sound for picture
        animateWithSound();

        // Build the action.
        Future<TakePicture> takePictureFuture = TakePictureBuilder.with(qiContext).buildAsync();

        Future<TimestampedImageHandle> timestampedImageHandleFuture = takePictureFuture.andThenCompose(takePicture -> {
            Log.i(TAG, "take picture launched!");
            return takePicture.async().run();
        });


        timestampedImageHandleFuture.andThenConsume(timestampedImageHandle -> {
            // Consume take picture action when it's ready
            Log.i(TAG, "Picture taken");
            // get picture
            EncodedImageHandle encodedImageHandle = timestampedImageHandle.getImage();

            EncodedImage encodedImage = encodedImageHandle.getValue();
            Log.i(TAG, "PICTURE RECEIVED!");

            // get the byte buffer and cast it to byte array
            ByteBuffer buffer = encodedImage.getData();
            buffer.rewind();
            final int pictureBufferSize = buffer.remaining();
            final byte[] pictureArray = new byte[pictureBufferSize];
            buffer.get(pictureArray);

            Log.i(TAG, "PICTURE RECEIVED! (" + pictureBufferSize + " Bytes)");
            // display picture
            pictureBitmap = BitmapFactory.decodeByteArray(pictureArray, 0, pictureBufferSize);
            runOnUiThread(() -> pictureView.setImageBitmap(pictureBitmap));

        });

    }

    @Override
    public void onRobotFocusLost() {

        // Stop the facial expression observation.
        facialExpressionObserver.stopObserving();

        // Remove the QiContext.
        this.qiContext = null;

        // Remove on started listeners from the animate action.
        if (animate != null) {
            animate.removeAllOnStartedListeners();
        }

        if (say != null) {
            say.removeAllOnStartedListeners();
        }

        if (mediaPlayer != null){
            mediaPlayer.release();
        }

    }

    @Override
    public void onRobotFocusRefused(String reason) {
        Log.e(TAG, "Robot focus refused : " + reason);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        QiSDK.unregister(this, this);

        // Stop listening to facial expression observer and remove it.
        facialExpressionObserver.setListener(null);
        facialExpressionObserver = null;

    }

}